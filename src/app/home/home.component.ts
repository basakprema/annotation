import { Component, OnInit, AfterViewInit } from '@angular/core';
import { fabric } from 'fabric';
import { from } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit,AfterViewInit {
  canvas:any;
  annotation ={
    height: '500',
    width: '500',
    coords: [
      {
        name:'option 1',
        height: '100',
        width: '600',
        dimensions: ['100', '100', '100', '100']
      },
      {
        name:'option 2',
        height: '100',
        width: '600',
        dimensions: ['200', '100', '100', '100']
      },
      {
        name:'option 3',
        height: '100',
        width: '600',
        dimensions: ['300', '100', '100', '100']
      }
          ]
  }
   
 
    

  constructor() { }
  onOptionClick(opt){
    var rect = new fabric.Rect({
      top: 10,
      left: 10,
      width: 60,
      height: 70,
      fill: 'red',
    });
    this.canvas.add(rect,0)
  }
ngAfterViewInit(){
  // var c: any = document.getElementById("canvas");
  // var canvas = new fabric.Canvas('canvas');
  this.canvas = new fabric.StaticCanvas('canvas');

  // var ctx = c.getContext("2d");
  fabric.Image.fromURL('http://fabricjs.com/assets/pug_small.jpg', function(myImg) {
    //i create an extra var for to change some image properties
    var img1 = myImg.set({ left: 0, top: 0 ,width:500,height:500});
    this.canvas.sendBackwards(img1); 
   });
  
}
  ngOnInit() {
      
      // var base_image = new Image();
      // base_image.src = './../../assets/docpng.png';
      // base_image.onload = function () {
      //   ctx.drawImage(base_image, 0, 0);
      // }
  
      // var context = c.getContext('2d');
      // context.beginPath();
      // context.rect(188, 50, 200, 100);
      // context.fillStyle = 'yellow';
      // context.fill();
      // var canvas = new fabric.Canvas('canvas');
      // var rect = new fabric.Rect({
      //   top: 10,
      //   left: 10,
      //   width: 60,
      //   height: 70,
      //   fill: 'red',
      // });
      // canvas.add(rect);

  }

}
